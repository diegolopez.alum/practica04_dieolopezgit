package Practica_03;

import java.util.Scanner;

/**
 * <h2>Clase La Calculadora, se utiliza para realizar las funciones de una calculadora </h2>
 * 
 * Busca informaci髇 de Javadoc en <a href="http://www.google.com">GOOGLE</a>
 * @version 3-2022
 * @author Diego Lopez
 * @since 01-03-2022
 */

public class LaCalculadora {
	
	/**
	 * Se instancia un scanner para que el usuario pueda ingresar los numeros desde el teclado para poder ejecutar las funciones de la la clase calculadora.
	 */
    static Scanner reader = new Scanner(System.in);
    

    /**
     * Metodo main 
     * @param args esta la informacion del String
     */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int op1 = 0, op2 = 0;
		char opcio;
		int resultat = 0;
		boolean control = false;

		do {
			mostrar_menu();
			opcio = llegirCar();

			switch (opcio) {
                case 'o':
                case 'O':
                    op1 = llegirEnter();
                    op2 = llegirEnter();
                    control = true;
                    break;
                case '+':
                    if (control)
                        resultat = suma(op1, op2);
                    else mostrarError();
                    break;
                case '-':
                    if (control)
                        resultat = resta(op1, op2);
                     else mostrarError();
                    break;
                case '*':
                    if (control)
                        resultat = multiplicacio(op1, op2);
                     else mostrarError();
                    break;
                case '/':
                    if (control) {
                        resultat = divisio(op1, op2);
                        if (resultat == -99)
                            System.out.print("No ha fet la divisi贸");
                        
                        else visualitzar(resultat);
                        
					}
		            else mostrarError();
                    
                    break;
                case 'v':
                case 'V':
                    if (control)
                        visualitzar(resultat);
                     else mostrarError();
                    break;
                case 's':
                case 'S':
                    System.out.print("Acabem.....");

                    break;
                default:
                    System.out.print("opci贸 erronia");
			}
			;
		} while (opcio != 's' && opcio != 'S');
		System.out.print("\nAdeu!!!");
		
		

	}
	
	/**
	 * Metodo para mostrar un mensaje de que ha habido un error al introducir los numeros.
	 */
    public static void mostrarError() { /* procediment */
		System.out.println("\nError, cal introduir primer els valors a operar");
	}



    /**
     * Metodo para poder sumar los numeros que sean ingresados desde la clase Scanner. 
     * @param a primer numero ingresado
     * @param b segundo numero ingresaod
     * @return nombre de la funcion int
     */
	public static int suma(int a, int b) { /* funci贸 */
		int res;
		res = a + b;
		return res;
	}
	
	/**
	 * Metodo para restar los numeros ingresados desde el Scanner
	 * @param a, primer numero ingresado
	 * @param b, segundo numero ingresado
	 * @return debolvera la resta de los otros dos numeros
	 */
	public static int resta(int a, int b) { /* funci贸 */
		int res;
		res = a - b;
		return res;
	}
	
	/**
	 * Metodo para multiplicar los numeros ingresados desde la clase Scanner
	 * @param a, primer numero ingresado
	 * @param b, segundo numero ingresado
	 * @return nos debolvera el resultado de la multiplicacio de los dos numeros
	 */
	public static int multiplicacio(int a, int b) { /* funci贸 */
		int res;
		res = a * b;
		return res;
	}
	
	/**
	 * Metodo para dividir dos numeros que ponga el usuario 
	 * @param a, primer numero que es ingresado
	 * @param b, segundo numero ingresado
	 * @return nos devolbera el resultado de la division si es correcta si no nos saltara un mensaje de error.
	 */
	public static int divisio(int a, int b) { /* funci贸 */
		int res = -99;
		char op;

		do {
			System.out.println("M. " + a + " mod " + b);
			System.out.println("D  " + a + " / " + b);
			op = llegirCar();
			if (op == 'M' || op == 'm'){
                if (b == 0) 
                    System.out.print("No es pot dividir entre 0\n");
                else
                    res = a % b;
			}

			else if (op == 'D' || op == 'd'){
                    if (b == 0)
                        System.out.print("No es pot dividir entre 0\n");
                    else
                        res = a / b;
                    }
                else
                    System.out.print("opci贸 incorrecte\n");
		} while (op != 'M' && op != 'm' && op != 'D' && op != 'd');

		return res;
	}
	
	/**
	 * Metodo para introducir un caracter
	 * @return devolvera el caracter ingresado
	 */
	public static char llegirCar() // funci贸
	{
		char car;

		System.out.print("Introdueix un car谩cter: ");
		car = reader.next().charAt(0);

		//reader.nextLine();
		return car;
	}
	
	/**
	 * Metodo para ingresar un numero entero
	 * @return nos devolvera el numero si es entero sino saldra un mensaje de error.
	 */
	public static int llegirEnter() // funci贸
	{
		int valor = 0;
		boolean valid = false;

		do {
			try {
				System.out.print("Introdueix un valor enter: ");
				valor = reader.nextInt();
				valid = true;
			} catch (Exception e) {
				System.out.print("Error, s'espera un valor enter");
				reader.nextLine();
			}
		} while (!valid);

		return valor;
	}
	
	/**
	 * Metodo para poder hacer una division entre zero
	 * @param num1 Priner numero ingresado
	 * @param num2 Segundo numero ingresado 
	 * @return Devolvera el resultado de la division
	 */
	public static int divideix(int num1, int num2) {
		int res = num1/num2;
		if (num2 == 0) {
			throw new java.lang.ArithmeticException("Divisio entre zero");
		} else res = num1/num2;
		return res; 	// DiegoLopez.
	}
	
	/**
	 * Metodo para ver el resultado de la operacion.
	 * @param res nombre de atributo para Int
	 */
	public static void visualitzar(int res) { /* procediment */
		System.out.println("\nEl resultat de l'operacio 茅s " + res);
	}
	
	/**
	 * Menu de opciones que tendra el usuario a la hora de ejecutar la calculadora
	 */
	public static void mostrar_menu() {
		System.out.println("\nCalculadora:\n");
		System.out.println("o.- Obtenir els valors");
		System.out.println("+.- Sumar");
		System.out.println("-.- Restar");
		System.out.println("*.- Multiplicar");
		System.out.println("/.- Dividir");
		System.out.println("v.- Visualitzar resultat");
		System.out.println("s.- Sortir");
		System.out.print("\n\nTria una opci贸 i ");
	}

}