import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ CalculadoraTestDivisio_Diego.class, CalculadoraTestMultiplicacio_Diego.class,
		CalculadoraTestResta_Diego.class, CalculadoraTestSuma_Diego.class })
public class AllTests {

}
